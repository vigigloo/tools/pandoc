ARG PANDOC_VERSION=2.18.0
FROM pandoc/latex:$PANDOC_VERSION

RUN tlmgr install --reinstall adjustbox babel-german background bidi collectbox csquotes everypage filehook footmisc \
    footnotebackref framed fvextra letltxmacro ly1 mdframed mweights needspace pagecolor sourcecodepro \
    sourcesanspro titling ucharcat ulem unicode-math upquote xecjk xurl zref
RUN tlmgr install --reinstall --repository https://www.komascript.de/repository/texlive/2021 koma-script

# Template Eisvogel
# https://github.com/Wandmalfarbe/pandoc-latex-template
RUN mkdir -p /pandoc-templates/eisvogel
RUN wget -O eisvogel.tar.gz https://github.com/Wandmalfarbe/pandoc-latex-template/releases/download/v2.0.0/Eisvogel-2.0.0.tar.gz
RUN tar -xzf eisvogel.tar.gz -C /pandoc-templates/eisvogel
RUN rm eisvogel.tar.gz
